import axios from 'axios';

const baseURL = 'http://legend-deee-tmzde1.apps.openforce.openforce.biz';
const userURL = `${baseURL}/users`;
const reportURL = `${baseURL}/reports`;
const suspectURL = `${baseURL}/suspects`;
const eventsURL = `${baseURL}/events`;
const baseIntURL =
	'http://intelligence-api-git-2-intelapp1.apps.openforce.openforce.biz/api';

export default {
	users: {
		login: (username, password) => {
			return axios.post(`${userURL}/login`, {
				username: username,
				password: password,
			});
		},
	},
	reports: {
		allReports: () => {
			return axios.get(`${reportURL}`);
		},
		allThresholds: () => {
			return axios.get(`${reportURL}/threshold`);
		},
		previousWeekReports: () => {
			return axios.get(`${reportURL}/previousWeek`);
		},
		updateThresholds: thresholdArray => {
			return axios.put(`${reportURL}/threshold`, { thresholdArray });
		},
		reportById: (type, id) => {
			return axios.post(`${reportURL}`, { type, id });
		},
	},
	suspects: {
		allSuspects: () => {
			return axios.get(`${suspectURL}`);
		},
		allWanted: () => {
			return axios.get(`${suspectURL}/wanted`);
		},
	},
	map: {
		getMap: () => {
			return axios.get(
				'http://maps-legend-git-tmzmap1.apps.openforce.openforce.biz/#/MapAmatz',
			);
		},
	},
	events: {
		getEvents: () => {
			return axios.get(`${eventsURL}/calendar`);
		},
		getTwoWeeksForecast: () => {
			return axios.get(`${eventsURL}/weather`);
		},
	},
	profile() {
		return {
			getLicenseById: async id => {
				return await axios.get(`${baseIntURL}/licenses/${id}`);
			},
			getNumOfPostsById: async id => {
				return await axios.get(`${baseIntURL}/posts/person/count/${id}`);
			},
			getProfileById: async id => {
				return await axios.get(`${baseIntURL}/suspects/suspect/${id}`);
			},
			getPermissionsById: async id => {
				return await axios.get(`${baseIntURL}/permissions/${id}`);
			},
			getPostsCounts: id => {
				return axios.get(`${baseIntURL}/posts/counts28/${id}`);
			},
		};
	},
};
