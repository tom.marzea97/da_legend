import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		suspectsAndWanted: {
			suspectsCount: 0,
			wantedCount: 0,
		},
	},
	mutations: {
		updateSuspectsCount(state, count) {
			state.suspectsAndWanted.suspectsCount = count;
		},
		updateWantedCount(state, count) {
			state.suspectsAndWanted.wantedCount = count;
		},
	},
	actions: {
		updateSuspectsCount({ commit }, count) {
			commit('updateSuspectsCount', count);
		},
		updateWantedCount({ commit }, count) {
			commit('updateWantedCount', count);
		},
	},
	getters: {
		getSuspectsAndWanted(state) {
			return state.suspectsAndWanted;
		},
	},
});

export default store;
