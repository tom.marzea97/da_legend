import Vue from "vue";
import vuetify from "@/plugins/vuetify"; // path to vuetify export
import App from "./App.vue";
import VueRouter from "vue-router";
import router from "./router";
import VCalendar from "v-calendar";
import Calendar from "v-calendar/lib/components/calendar.umd";
import DatePicker from "v-calendar/lib/components/date-picker.umd";
import "material-design-icons-iconfont/dist/material-design-icons.css";
import store from "./store";

Vue.use(vuetify);
Vue.component("calendar", Calendar);
Vue.component("date-picker", DatePicker);
Vue.use(VueRouter);
Vue.use(VCalendar);

Vue.config.productionTip = false;

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount("#app");
